import { DirCreate } from '@/types/dir-create'

export class DirFormModel {
  name = ''

  static default (): DirFormModel {
    return new this()
  }

  toDto (dirId: number): DirCreate {
    return {
      dirId,
      name: this.name
    }
  }
}
