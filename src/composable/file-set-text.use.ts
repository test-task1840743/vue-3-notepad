﻿import { useStore } from 'vuex'
import { FileContent } from '@/types/file-content'

export const fileSetTextUse = () => {
  const { dispatch } = useStore()
  const setText = (dto: FileContent) => {
    return dispatch('setTextFile', dto)
  }
  return {
    fileSetText: setText
  }
}
