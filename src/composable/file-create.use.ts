﻿import { useStore } from 'vuex'
import { FileCreate } from '@/types/file-create'

export const fileCreateUse = () => {
  const { dispatch } = useStore()
  const create = (dto: FileCreate) => {
    return dispatch('createFile', dto)
  }
  return {
    fileCreate: create
  }
}
