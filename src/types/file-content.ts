export interface FileContent {
  id: number
  content: string
}
