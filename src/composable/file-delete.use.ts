﻿import { useStore } from 'vuex'

export const fileDeleteUse = () => {
  const { dispatch } = useStore()
  const del = (id: number) => {
    return dispatch('deleteFile', id)
  }
  return {
    fileDelete: del
  }
}
