﻿import { useStore } from 'vuex'
import { DirCreate } from '@/types/dir-create'

export const dirCreateUse = () => {
  const { dispatch } = useStore()
  const create = (dto: DirCreate) => {
    return dispatch('createDir', dto)
  }
  return {
    dirCreate: create
  }
}
