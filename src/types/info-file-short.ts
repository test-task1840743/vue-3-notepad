export interface InfoFileShort {
  id: number
  path: string
  name: string
}
