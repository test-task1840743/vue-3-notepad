import { createStore } from 'vuex'
import { FileContent } from '@/types/file-content'
import { TreeItem } from '@/types/tree-item'
import { FileCreate } from '@/types/file-create'
import { DirCreate } from '@/types/dir-create'

export default createStore<{
  tree: TreeItem[],
  fileContents: FileContent[]
}>({
  state: {
    tree: [
      { id: 0, type: 'dir', name: 'root-dir', children: [] }
    ],
    fileContents: []
  },
  mutations: {
    createFile (state, { payload, id }: {payload: FileCreate, id: number}) {
      state.tree.push({ id, type: 'file', name: payload.name, children: [] })
      const item = state.tree.find((item) => item.id === payload.dirId)!
      item.children.push(id)
      return id
    },
    deleteItem (state, id: number) {
      const itemIndex = state.tree.findIndex((item) => item.id === id)!
      const itemParentIndex = state.tree.findIndex((item) => item.children.includes(id))!
      if (itemParentIndex !== -1) {
        const itemParentChildIndex = state.tree[itemParentIndex].children.findIndex((item) => item === id)
        state.tree[itemParentIndex].children.splice(itemParentChildIndex, 1)
      }

      function cascade (tree: TreeItem[], index: number): number[] {
        return tree[index].children?.map((item) => {
          const i = tree.findIndex((el) => el.id === item)
          const res = cascade(tree, i)
          return [item, ...res]
        })?.flat() || []
      }

      cascade(state.tree, itemIndex).forEach((deleteId) => {
        const index = state.tree.findIndex((el) => el.id === deleteId)
        state.tree.splice(index, 1)
      })

      state.tree.splice(itemIndex, 1)
    },
    createDir (state, payload: DirCreate) {
      const id = Math.floor(Math.random() * 100000) + 100
      state.tree.push({ id, type: 'dir', name: payload.name, children: [] })
      const item = state.tree.find((item) => item.id === payload.dirId)!
      item.children = [id, ...item.children]
      return id
    },
    setTextFile (state, payload: FileContent) {
      const item = state.fileContents.find((item) => item.id === payload.id)
      if (!item) {
        state.fileContents.push({ ...payload })
        return
      }
      item.content = payload.content
    }
  },
  actions: {
    createFile (context, payload: FileCreate) {
      const id = Math.floor(Math.random() * 100000) + 100
      context.commit('createFile', { payload, id })
      return id
    },
    deleteFile (context, id) {
      context.commit('deleteItem', id)
      return id
    },
    createDir (context, payload: DirCreate) {
      context.commit('createDir', payload)
    },
    setTextFile (context, payload: FileContent) {
      context.commit('setTextFile', payload)
    }
  },
  modules: {
  }
})
