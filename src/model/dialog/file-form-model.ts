import { FileCreate } from '@/types/file-create'

export class FileFormModel {
  name = ''

  static default (): FileFormModel {
    return new this()
  }

  toDto (dirId: number): FileCreate {
    return {
      dirId,
      name: this.name
    }
  }
}
