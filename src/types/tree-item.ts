
export interface TreeItem {
  id: number;
  type: string;
  name: string;
  children: number[];
}
